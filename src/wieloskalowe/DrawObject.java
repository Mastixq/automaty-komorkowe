/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wieloskalowe;

/**
 *
 * @author Michał
 */
public class DrawObject {
        int sizeX, sizeY;
    int grid[][];
    int p1,p2;
    int x1,x2,y1,y2;
    DrawObject(int grid[][], int sizeX, int sizeY, int p1, int p2){
        this.grid=grid;
        this.sizeX=sizeX;
        this.sizeY=sizeY;
        this.p1=p1;
        this.p2=p2;
    }
    
    void glider(){
        int drawX, drawY;
        int gliderGrid[][]={{0,1,0},
                            {0,0,1},
                            {1,1,1}};
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                if(p1+i>sizeX-1)
                    drawX=(p1+i)-sizeX;
                else 
                    drawX=p1+i;
                if(p2+j>sizeY-1)
                    drawY=(p2+j)-sizeY;
                else 
                    drawY=p2+j;
                grid[drawX][drawY]=gliderGrid[i][j];
            }
        }
    }
    void canon(){
        int drawX, drawY;
        int canonGrid[][]={{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0},
                            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0},
                            {0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1},
                            {0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1},
                            {1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                            {1,1,0,0,0,0,0,0,0,0,1,0,0,0,1,0,1,1,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0},
                            {0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0},
                            {0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                            {0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}};
        for(int i=0;i<9;i++){
            for(int j=0;j<36;j++){
                if(p1+i>sizeX-1)
                    drawX=(p1+i)-sizeX;
                else 
                    drawX=p1+i;
                if(p2+j>sizeY-1)
                    drawY=(p2+j)-sizeY;
                else 
                    drawY=p2+j;
                grid[drawX][drawY]=canonGrid[i][j];
            }
        }
    }
}
