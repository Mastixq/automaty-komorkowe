package wieloskalowe;
/**
 *
 * @author Michał
 */
import org.lwjgl.*;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;
import org.lwjgl.system.*;
import java.nio.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.system.MemoryUtil.*;

public class Wieloskalowe {
    public static void main(String[] args) {
		//new Wieloskalowe().run();  
                new Menu();
	}
    Wieloskalowe(int sizeX, int sizeY){
        this.sizeX=sizeX;
        this.sizeY=sizeY;
        grid=new int[sizeX][sizeY];
        borderX=2f/sizeX;
        borderY=2f/sizeY;
        System.out.println("konstruktor");
    }
	// The window handle
	private long window;
        
        int windowWidth=500;
        int windowHeight = 500;
        
        float color=0.5f;
        
        int sizeX;
        int sizeY;
        int grid [][];
        
        float borderX;
        float borderY=2f/sizeY;
        boolean gridFlag=false;
        boolean gameFlag=false;
        boolean onceFlag=false;
        
        int mouseX, mouseY;
        DrawObject testObject;
        
	public void run() {
        	System.out.println("Hello LWJGL " + Version.getVersion() + "!");
		init();
		loop();

		// Free the window callbacks and destroy the window
		glfwFreeCallbacks(window);
		glfwDestroyWindow(window);

		// Terminate GLFW and free the error callback
		glfwTerminate();
		glfwSetErrorCallback(null).free();
	}

	private void init() {
		// Setup an error callback. The default implementation
		// will print the error message in System.err.
		GLFWErrorCallback.createPrint(System.err).set();

		// Initialize GLFW. Most GLFW functions will not work before doing this.
		if ( !glfwInit() )
			throw new IllegalStateException("Unable to initialize GLFW");

		// Configure GLFW
		glfwDefaultWindowHints(); // optional, the current window hints are already the default
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable
                
		// Create the window
		window = glfwCreateWindow(windowWidth, windowHeight, "Game of life!",  NULL, NULL);  //null nie wlacza full screena glfwGetPrimaryMonitor()
		if ( window == NULL )
			throw new RuntimeException("Failed to create the GLFW window");

		// Setup a key callback. It will be called every time a key is pressed, repeated or released.
		glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
			if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
				glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
                        if ( key == GLFW_KEY_Q && action == GLFW_RELEASE )
				gridFlag=!gridFlag;
                        if ( key == GLFW_KEY_W && action == GLFW_RELEASE )
				onceFlag=true;
                        if ( key == GLFW_KEY_E && action == GLFW_RELEASE ){
                                gameFlag=!gameFlag;
                        }
                        if ( key == GLFW_KEY_C && action == GLFW_RELEASE ){
                           for(int i=0; i<sizeX; i++){
                               for(int j=0;j<sizeY;j++)
                                  grid[i][j]=0; 
                           } 
                        }
                        if ( key == GLFW_KEY_T && action == GLFW_RELEASE ){
                            getMouse();
                            testObject=new DrawObject(grid,sizeX,sizeY,mouseX,mouseY);
                            testObject.glider();
                        }
                        if ( key == GLFW_KEY_Y && action == GLFW_RELEASE ){
                            getMouse();
                            testObject=new DrawObject(grid,sizeX,sizeY,mouseX,mouseY);
                            testObject.canon();
                        }
                });
                
                int button = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1);

		// Get the thread stack and push a new frame
		try ( MemoryStack stack = stackPush() ) {
			IntBuffer pWidth = stack.mallocInt(1); // int*
			IntBuffer pHeight = stack.mallocInt(1); // int*

			// Get the window size passed to glfwCreateWindow
			glfwGetWindowSize(window, pWidth, pHeight);

			// Get the resolution of the primary monitor
			GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

			// Center the window
			glfwSetWindowPos(
				window,
				(vidmode.width() - pWidth.get(0)) / 2,
				(vidmode.height() - pHeight.get(0)) / 2
			);
		} // the stack frame is popped automatically

		// Make the OpenGL context current
		glfwMakeContextCurrent(window);
		// Enable v-sync
		glfwSwapInterval(1);

		// Make the window visible
		glfwShowWindow(window);
	}
        public void display(){
                        //display();
                        IntBuffer w = BufferUtils.createIntBuffer(1);
                        IntBuffer h = BufferUtils.createIntBuffer(1);
                        glfwGetWindowSize(window, w, h);
                        glfwMakeContextCurrent(window);
                        if(windowWidth!= w.get(0) || windowHeight != h.get(0)){
                            windowWidth = w.get(0);
                            windowHeight = h.get(0);
                            glfwFreeCallbacks(window);
                            glfwDestroyWindow(window);
                            init();
                        }
                        for(int i=0; i<sizeX; i++){
                            for(int j=0; j<sizeY; j++){
                                glColor3f(105f/255f, 105f/255f, 105f/255f);
                                if (grid[i][j]==1)
                                    glColor3f(255f/255f, 255f/255f, 255f/255f);
                                //quad()
                                float x1=-1+borderX*i;
                                float x2=-1+borderX+borderX*i;
                                float y1=-1+borderY*j;
                                float y2=-1f+borderY+borderY*j;
                                quad(x1,x2,y1,y2);
                                
                        }
                    }
}
	private void loop() {
		GL.createCapabilities();
                
                GameOfLife game=new GameOfLife(grid,sizeX,sizeY);
		while ( !glfwWindowShouldClose(window) ) {
                           int button = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1);
                if(gameFlag)
                    game.calculateStep();
               
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Wieloskalowe.class.getName()).log(Level.SEVERE, null, ex);
                    }
                
                if(button>0)
                    mouseDraw();
                display();
                if(gridFlag)
                    gridDraw();
                
                if(onceFlag){
                    game.calculateStep();
                    onceFlag=false;
                    gameFlag=false;
                }
                    glfwSwapBuffers(window); // swap the color buffers
                    glfwPollEvents();
		}
	}
        
        void quad(float x1, float x2, float y1, float y2){
	glBegin(GL_QUADS);
        float check=0;
            glVertex3f(x1, check-y1, 0.0f);
            glVertex3f(x2, check-y1, 0.0f);
            glVertex3f(x2, check-y2, 0.0f);
            glVertex3f(x1, check-y2, 0.0f);
	glEnd();
    }
        void lineX(float x1){
            glColor3f(0f, 0.0f, 0.f);
                glBegin(GL_LINES);
                glVertex3f(-1+x1,-1,0);
                glVertex3f(-1+x1,1,0);
                glEnd();
        }
        void lineY(float x1){
                glColor3f(0f, 0.0f, 0.f);
                glBegin(GL_LINES);
                glVertex3f(-1,-1+x1,0);
                glVertex3f(1,-1+x1,0);
                glEnd();
        }
        void mouseDraw(){
            getMouse();        
            grid[mouseX][mouseY]=1;
        }
        void getMouse(){
            DoubleBuffer b1 = BufferUtils.createDoubleBuffer(1);
            DoubleBuffer b2 = BufferUtils.createDoubleBuffer(1);
            glfwGetCursorPos(window, b1, b2);
            int oldX=mouseX;
            int oldY=mouseY;
            mouseX=(int)(b1.get(0)/((double)windowWidth/sizeX));
            mouseY=(int)(b2.get(0)/((double)windowHeight/sizeY));
            if(mouseX>sizeX || mouseX<0|| mouseY>sizeY || mouseY<0){
                mouseX=oldX;
                mouseY=oldY;
            } 
        }
        void gridDraw(){
            for(int i=1;i<=sizeX;i++){
                lineX(borderX*i);
                lineY(borderY*i);
            } 
        }
}