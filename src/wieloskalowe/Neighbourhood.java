package wieloskalowe;

/**
 *
 * @author Michał
 */
public class Neighbourhood {
    int sizeX, sizeY;
    int grid[][];
    int p1,p2;
    int x1,x2,y1,y2;
    int N[]=new int[8];//od gory po lewej po wskazowkach zegara
    Neighbourhood(int grid[][], int sizeX, int sizeY, int p1, int p2){
        this.grid=grid;
        this.sizeX=sizeX;
        this.sizeY=sizeY;
        this.p1=p1;
        this.p2=p2;
        calc();
    }
    void calc(){
        if(p1-1<0)
            x1=sizeX-1;
        else 
            x1=p1-1;//ok
        if(p1+1>sizeX-1)
            x2=0;
        else 
            x2=p1+1;
        
        
        if(p2-1<0)
            y1=sizeY-1;
        else 
            y1=p2-1;  
        if(p2+1>sizeY-1)
            y2=0;
        else 
            y2=p2+1;         
    }
    int Moore(){
        int counter=0;
        N[0]=grid[x1][y1];
        N[1]=grid[x1][p2]; 
        N[2]=grid[x1][y2]; 
        N[3]=grid[p1][y1]; 
        N[4]=grid[p1][y2]; 
        N[5]=grid[x2][y1]; 
        N[6]=grid[x2][p2]; 
        N[7]=grid[x2][y2]; 
        for(int i=0;i<8;i++)
            counter+=N[i];
    return counter;    
    }
    
}
