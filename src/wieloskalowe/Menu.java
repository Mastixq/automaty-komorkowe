package wieloskalowe;
/**
 *
 * @author Michał
 */
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javafx.beans.value.ObservableValue;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author Michał
 */
public class Menu extends JFrame {
    public JButton start;
    public static final int height = 400;
    public static final int width = 200;
    static final int X_MIN = 5;
    static final int X_MAX = 255;
    static final int Y_MIN = 5;
    static final int Y_MAX = 255;
    static final int INIT = 125; 
    public int currX=INIT;
    public int currY=INIT;
    JSlider YSlider;
    JSlider XSlider;
    JLabel textX;
    JLabel textY;
    public JLayeredPane lp;
    HandlerClass handler;
    MyChangeListener listener;
    Menu(){
        super("Wieloskalowe");
        int screenWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
        int screenHeight = Toolkit.getDefaultToolkit().getScreenSize().height;
        // Ustawienia ramki
        setBounds(screenWidth/2 - width/2, (screenHeight/2-height/2), width, height);
        setSize(width,height);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        
        textX=new JLabel("x: "+INIT);
        textX.setBounds(width/2-170/2, 70
                , 170, 50);
        XSlider=new JSlider(JSlider.HORIZONTAL,X_MIN, X_MAX, INIT);
        XSlider.setBounds(width/2-170/2, 110, 170, 50);

//Turn on labels at major tick marks.
        XSlider.setMajorTickSpacing(50);
        XSlider.setMinorTickSpacing(1);
        XSlider.setPaintLabels(true);
        Font font = new Font("Serif", Font.ITALIC, 12);
        XSlider.setFont(font);
        
        
        //y
        textY=new JLabel("y: "+INIT);
        textY.setBounds(width/2-170/2, 160, 170, 50);
        YSlider=new JSlider(JSlider.HORIZONTAL,Y_MIN, Y_MAX, INIT);
        YSlider.setBounds(width/2-170/2, 200, 170, 50);

//Turn on labels at major tick marks.
        YSlider.setMajorTickSpacing(50);
        YSlider.setMinorTickSpacing(12);
        YSlider.setPaintLabels(true);
        YSlider.setFont(font);
                //baza
        lp = getLayeredPane();
        
        start = new JButton("Start");
        start.setBounds(width/2-50, 10, 100, 25);
        lp.add(start);
        lp.add(textX);
        lp.add(textY);
        lp.add(XSlider);
        lp.add(YSlider);
        handler = new HandlerClass();
        listener=new MyChangeListener();
        start.addActionListener(handler);
        XSlider.addChangeListener(listener);
        YSlider.addChangeListener(listener);
    }
    private class HandlerClass implements ActionListener {
      @Override
      public void actionPerformed(ActionEvent event){
          //losuj
          if (event.getSource()==start){
                new Wieloskalowe(currX,currY).run();
                System.out.println(currX);
                //game.run();
          }
      }
      
    }
 class MyChangeListener implements ChangeListener {
    MyChangeListener() {
    }

    @Override
    public synchronized void stateChanged(ChangeEvent e) {
      int x = XSlider.getValue();
      currX=x;
      textX.setText("x: " + x);
      int y = YSlider.getValue();
      currY=y;
      textY.setText("y: " + y);
    }
  }
}
