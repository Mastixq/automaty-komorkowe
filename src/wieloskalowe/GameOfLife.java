package wieloskalowe;
/**
 *
 * @author Michał
 */
public class GameOfLife {
    int sizeX, sizeY;
    int grid[][];
    int grid2[][];
    GameOfLife(int grid[][], int sizeX,int sizeY){
        this.sizeX=sizeX;
        this.sizeY=sizeY;
        this.grid=grid;
        grid2=new int[sizeX][sizeY];
    } 
    void calculateStep(){
        for(int i=0; i<sizeX; i++){
                for(int j=0;j<sizeY;j++){
                    grid2[i][j]=grid[i][j];
                }
        }
        for(int i=0; i<sizeX; i++){
            for(int j=0; j<sizeY; j++){
                Neighbourhood moore=new Neighbourhood(grid,sizeX,sizeY,i,j);
                int q=moore.Moore();
                if(grid[i][j]==0){
                    if(q==3)
                        grid2[i][j]=1;
                    }
                else{
                    if(q<2 || q>3)
                     grid2[i][j]=0;   
                }
            }
        }
        for(int i=0; i<sizeX; i++){
            for(int j=0;j<sizeY;j++){
                    grid[i][j]=grid2[i][j];
                }
        }
    }
}
